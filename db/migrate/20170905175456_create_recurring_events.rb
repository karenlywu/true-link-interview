class CreateRecurringEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :recurring_events do |t|
      t.column :name, :string, :null => false
      t.column :start_date, :date
      t.column :months_between_occurrences, :integer
      t.column :day_of_month, :integer
      t.column :num_buffer_days, :integer
      t.timestamps
    end
  end
end
