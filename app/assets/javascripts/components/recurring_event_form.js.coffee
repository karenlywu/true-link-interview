@RecurringEventForm = React.createClass

  getInitialState: ->
    name: ''
    start_date: ''
    months_between_occurrences: 1 #default this to 1, because that is the minimum
    day_of_month: 1 #defaulting to 1 for now, that's the lowest it can go
    num_buffer_days: 0 #defaulting to 0, can be at least on the date

  handleValueChange: (e) ->
        valueName = e.target.name
        @setState "#{ valueName }" : e.target.value

  handleSubmit: (e) ->
    e.preventDefault()
    $.post '', {recurring_event: @state}, (data) =>
      #@props.handleNewRecurringEvent data
      @setState @getInitialState()
    , 'JSON'

  isValid: ->
    @state.name && @state.start_date # since these have no default values and are required

  render: ->
      React.DOM.form
        onSubmit: @handleSubmit
        React.DOM.div
          className: 'form-group'
          React.DOM.label
            'Name'
          React.DOM.input
            type: 'text'
            className: 'form-control'
            placeholder: 'name'
            name: 'name'
            label: 'name'
            value: @state.name
            onChange: @handleValueChange
        React.DOM.div
          className: 'form-group'
          React.DOM.label
            'Start Date'
          React.DOM.input
            type: 'date'
            className: 'form-control'
            placeholder: 'start date'
            name: 'start_date'
            value: @state.start_date
            onChange: @handleValueChange
        React.DOM.div
          className: 'form-group'
          React.DOM.label
            'Number of Months between occurences'
          React.DOM.input
            type: 'number'
            min: 1
            className: 'form-control'
            placeholder: 'The number of months between occurrences'
            name: 'months_between_occurrences'
            value: @state.months_between_occurrences
            onChange: @handleValueChange        
        React.DOM.div
          className: 'form-group'
          React.DOM.label
            'Day of the Month'
          React.DOM.input
            type: 'number'
            className: 'form-control'
            placeholder: 'day of the month for transaction'
            name: 'day_of_month'
            value: @state.day_of_month
            onChange: @handleValueChange
        React.DOM.div
          className: 'form-group'
          React.DOM.label
            'Number of buffer days'
          React.DOM.input
            type: 'number'
            className: 'form-control'
            placeholder: 'number of buffer days'
            name: 'num_buffer_days'
            value: @state.num_of_buffer_days
            onChange: @handleValueChange
        React.DOM.div
          className: 'form-group'
          React.DOM.button
            type: 'submit'
            className: 'btn btn-primary'
            disabled: !@isValid()
            'Submit'


