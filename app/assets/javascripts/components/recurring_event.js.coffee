@RecurringEvent = React.createClass
  
  getInitialState: ->
    name: @props.recurring_event.name
    #start_date: @props.recurring_event.start_date #will change to calculate later
    deliveryDates: @props.next_delivery_dates

  render: ->
    React.DOM.tr null,
      React.DOM.td null, @state.name
      React.DOM.td null, @state.deliveryDates[0]
      React.DOM.td null, @state.deliveryDates[1]
      React.DOM.td null, @state.deliveryDates[2]
      React.DOM.td null, @state.deliveryDates[3]


  