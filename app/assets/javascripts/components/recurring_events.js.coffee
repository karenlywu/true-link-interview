@RecurringEvents = React.createClass

  getInitialState: ->
    recurring_events: @props.data
    next_delivery_dates: @props.next_delivery_dates

  render: ->
    React.DOM.div
      className: 'recurring_events'
      React.DOM.h1
        className: 'title'
        'Recurring Events'
      React.createElement RecurringEventForm
        React.DOM.table
          className: 'table table-bordered'
          React.DOM.thead null,
            React.DOM.tr null,
              React.DOM.th null, 'Name'
              React.DOM.th null, 'Next Delivery Date 1'
              React.DOM.th null, 'Next Delivery Date 2'
              React.DOM.th null, 'Next Delivery Date 3'
              React.DOM.th null, 'Next Delivery Date 4'              
          React.DOM.tbody null,
            for recurring_event in @state.recurring_events
                React.createElement RecurringEvent, key: recurring_event.id, recurring_event: recurring_event, next_delivery_dates: @props.next_delivery_dates[recurring_event.name]
