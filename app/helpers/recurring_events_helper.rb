require 'date'

module RecurringEventsHelper

	holidays = [ Date.new(2017, 10, 9), Date.new(2017, 11, 11), Date.new(2017, 11, 23), Date.new(2017, 12, 25),
		Date.new(2018, 1, 1), Date.new(2018, 1, 15), Date.new(2018, 2, 19), Date.new(2018, 5, 28), Date.new(2018, 7, 4),
		Date.new(2018, 9, 3), Date.new(2018, 10, 8), Date.new(2018, 11, 12), Date.new(2018, 11, 22), Date.new(2018, 12, 25)
	]
	# list of holidays from now till 2018 listed on the provided site https://www.frbservices.org/holidayschedules/
  	# but for future reference would probably try to find a holiday api and hook that up
	def next_delivery_date(start_date, day_of_month, num_buffer_days, months_between_occurrences)

		# auto fill for nil vars
		if num_buffer_days == nil
			num_buffer_days = 0			
		end

		if months_between_occurrences == nil
			months_between_occurrences = 1
		end

		date = start_date

		# check with today
		#if date < Date.today
		#	date = Date.today
		#end

		# if the start date's day is greater than day of the month
		# then we want to start at the next month
		if date.day > day_of_month
			# change the month
			date = date >> months_between_occurrences
			# change to the correct day
			date = Date.new(date.year, date.month, day_of_month)
			#end
		end

		# check if it's still a business day
		if date.saturday?
			date = date -1
		end

		if date.sunday?
			date = date -2
		end

		# if there aren't any buffer days lets just get outta here
		if num_buffer_days == 0
			return date
		end

		due_date = Date.new(date.year, date.month, day_of_month)

		# calculate number of buffer days
		business_days = 0
		_date = due_date
		while _date > date
		   	business_days = business_days + 1 unless _date.saturday? or _date.sunday?
		   	_date = _date - 1
		end

		# checks

		
		if num_buffer_days != business_days
				difference = num_buffer_days - business_days
				#take the difference
				if difference < 0
					# add abs(diffence) business days to date
					count = difference
					while count > 0
						date = date + 1
						if !date.saturday? and !date.sunday?
							count = count - 1
						end
					end
				else
					# subtract abs(difference in business days) business days from date
					count = difference
					while count > 0
						date = date - 1
						if !date.saturday? and !date.sunday?
							count = count - 1
						end
					end
				end
			

		end
		return date
	end
	module_function :next_delivery_date


	# TODO : 
	# keep getting the same answers, realize i might need a field to keep track of due dates or next due date
	# because this is different delivery date and can cause a loop of answers
	def next_X_delivery_dates(num, start_date, day_of_month, num_buffer_days, months_between_occurrences)
		@delivery_dates = []
		#new_start_date = start_date
		count = num
		date = start_date

		loop do	
			date = get_next_due_date(date, day_of_month)
			@delivery_dates.push(next_delivery_date(date, day_of_month, num_buffer_days, months_between_occurrences))
			count = count - 1
			if count == 0
				break
			end
		end
		return @delivery_dates
	end
	module_function :next_X_delivery_dates

	def get_next_due_date(date, day_of_month)
		due_date = date
		due_date = due_date >> 1 # set to next month
		due_date = Date.new(due_date.year, due_date.month, day_of_month) # set to the correct day
		puts "due date : #{due_date}"
		return due_date
	end
	module_function :get_next_due_date
end

# test cases
def test_with_months_of_occurrence
	puts " ----------- Testing with months of ocurrence field -------------- "
	delivery_date_1 = RecurringEventsHelper.next_delivery_date(Date.new(2017, 9, 9), 3, 0, 1)
	puts "should be : 10/03/17 #{delivery_date_1.to_s}"
	delivery_date_2 = RecurringEventsHelper.next_delivery_date(Date.new(2017, 10, 9), 3, 0, 2)
	puts "should be : 12/01/17 #{delivery_date_2.to_s}"
	delivery_date_3 = RecurringEventsHelper.next_delivery_date(Date.new(2017, 11, 8), 7, 0, 3)
	puts "should be : 02/07/17 #{delivery_date_3.to_s}"
end

def test_with_buffer_days
	puts " ----------- Testing with buffer days field -------------- "
	delivery_date_1 = RecurringEventsHelper.next_delivery_date(Date.new(2017, 9, 9), 3, 1, 1)
	puts "should be : 10/02/17 #{delivery_date_1.to_s}"
	delivery_date_2 = RecurringEventsHelper.next_delivery_date(Date.new(2017, 10, 9), 3, 2, 1)
	puts "should be : 11/01/17 #{delivery_date_2.to_s}"
	delivery_date_3 = RecurringEventsHelper.next_delivery_date(Date.new(2017, 11, 8), 7, 3, 1)
	puts "should be : 12/04/17 #{delivery_date_3.to_s}"
end

test_with_months_of_occurrence
test_with_buffer_days
