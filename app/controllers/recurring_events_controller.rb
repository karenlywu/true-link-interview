class RecurringEventsController < ApplicationController	
	def index 
		@next_delivery_dates = {}
		@recurring_events = RecurringEvent.all
		@recurring_events.each do |recurring_event|
			get_next_x_delivery_dates recurring_event
		end
		sleep(5)
	end

	def create
		@recurring_event = RecurringEvent.new(recurring_event_params)

		if @recurring_event.save
			render json: @recurring_event
		else
			render json: @recurring_event.errors, status: :unprocessable_entity
		end
	end

	private
	def recurring_event_params
		params.require(:recurring_event).permit(:name, :start_date, :months_between_occurrences, :day_of_month, :num_buffer_days)
	end

	def get_next_x_delivery_dates (recurring_event)
		@next_delivery_dates.merge!(recurring_event.name => RecurringEventsHelper.next_X_delivery_dates(4, recurring_event.start_date, recurring_event.day_of_month, recurring_event.num_buffer_days, recurring_event.months_between_occurrences))
	end
end
